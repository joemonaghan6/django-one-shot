from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


class TodosListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodosDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todosdetail"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["items"] = TodoItem()
    #     return context


class TodoListCreateView(LoginRequiredMixin, CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_list")
