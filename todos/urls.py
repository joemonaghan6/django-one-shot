from django.urls import path
from todos.views import TodosListView, TodosDetailView, TodoListCreateView


urlpatterns = [
    path("", TodosListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodosDetailView.as_view(), name="todo_list_detail"),
    path("new/", TodoListCreateView.as_view(), name="todo_list_new"),
]
