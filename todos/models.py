from django.db import models


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True, blank=True
    )  # the field can be empty
    is_completed = False
    list = models.ForeignKey(
        "TodoList",  # This connects us to the other model
        related_name="items",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.task)
