from django.contrib import admin
from django.urls import include, path, reverse_lazy
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views


urlpatterns = [
    path("admin/", admin.site.urls),
    path("todos/", include("todos.urls")),
    path(
        "",
        RedirectView.as_view(
            url=reverse_lazy("todo_list_list")
        ),  # todos is the main site and this is telling it to show the list.html in todos
        name="home",
    ),
]
